# Poland plants recognition DNN

ResNet 101 DNN model trained with [mxnet](https://github.com/dmlc/mxnet) for recognition of  plants occuring in Poland. 

Live working application at [Atlas roślin](https://atlas.roslin.pl/rozpoznaj_zdjecie) (PL) page.

## Accuracy

Test set accuracy over 5860 images:


| Place | Accuracy |
| ----- | -------- |
| top1  | 0.7867   |
| top3  | 0.8842   |
| top5  | 0.9139   |
| top10 | 0.9421   |
| top15 | 0.9543   |
| top20 | 0.9616   |

## Synset 

Synset relates to plant id's in our database, if you need plant latin names - write me here (issue). 


## Licence

GNU PL
